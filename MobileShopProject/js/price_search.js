// 获取 ul
let oul = document.querySelector("#ulbox")
// 接收 标题的 titleId
let titleArr = [];
// 接收 每一个div中的内容
let contentArr = [];

// 发送请求 一级 标题    同步
const xhr1 = new XMLHttpRequest();
xhr1.open("get","http://chst.vip:1234/api/getcategorytitle",false);
xhr1.onreadystatechange = function(){
    if(xhr1.readyState === 4){
        if(xhr1.status === 200){
            let res1 = JSON.parse(xhr1.response);
            let ele = "";
            res1.result.forEach(item=>{
                // 把每个 titleId 存起来
                titleArr.push(item.titleId);
                // 拼接 每一个 一级标题的 结构
                ele += `<li>
                            <h4>${item.title}</h4>
                            <div></div>
                        </li>`
            });
            // 写入到 ul中
            oul.innerHTML = ele;
        }
    }
}
xhr1.send(null)

// 发送 二级内容的 请求   同步      value是每个 titleId
titleArr.forEach(value=>{
    const xhr2 = new XMLHttpRequest();
    xhr2.open("get","http://chst.vip:1234/api/getcategory?titleid="+value,false);
    xhr2.onreadystatechange = function(){
        if(xhr2.readyState === 4){
            if(xhr2.status === 200){
                let res2 = JSON.parse(xhr2.response);
                console.log(res2);
                let divContent = ""
                res2.result.forEach(item => {
                    divContent +=`<P>${item.category}</P>`          
                });
                // 把 每个 div中内容 添加到 数组中
                contentArr.push(divContent);
                // 清空 为下次 拼接 内容准备
                divContent = ""
            }
        }
    }
    xhr2.send(null)
})

// 获取到 每个 div 给 div中 写入 存在数组中的 内容
let aDiv = document.querySelectorAll("#ulbox li div")
aDiv.forEach((item,index)=>{
    console.log(item)
    console.log(contentArr[index])
    item.innerHTML = contentArr[index];
});



//  slideDown   展开高度      自动动画效果
//  slideUp     收起高度      自动动画效果

// console.log($("#ulbox"))
// console.log($("#ulbox").find("div"))

$("#ulbox").find("div").slideUp(0,function(){
    $("#ulbox").css("display","block");
});


// console.log($("#ulbox li"))

$("#ulbox li").click(function(){
    // console.log($(this))
    
    $(this).find("div").slideDown().parent().siblings().find("div").slideUp();

    // $("#ulbox li").find("div").slideUp();
    // $(this).find("div").slideDown();
    
})